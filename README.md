# OpenZen Binary Libraries

This repository contains binary libraries used by some build options of the OpenZen library <https://bitbucket.org/lpresearch/openzen/>.

They are located in this separate repository to ensure only files complying with Open Sources licenses are contained in the main OpenZen repostory.

Please see the LICENSE file for the contained binaries and their respective libraries.
